This work aims to show the relationship between physical aspects and the occurrence of endemic species of the Brazilian semi-arid 
region through ecological niche modeling technique aided by remote sensing products. It is an interdisciplinary project, integrating Brazilian and Spanish research centers in the 
study fields of biogeography, remote sensing, statistics and computer science. We use the R programming language and DISMO Package to execute this experiment.

We use the Ecological Niche Modeling to identify the Habitat Adequability of Ziziphus Joaneiro specie in the Brazilian Semi-arid Region.

While the ENM execution, the provenance metadata are recorded through the BioProv Package, developed by our group.